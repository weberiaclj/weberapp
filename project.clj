(defproject weberapp "1.0.0"
  :description "Weberia application examples"
  :url "http://github.com/weberia/weberapp"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]])
